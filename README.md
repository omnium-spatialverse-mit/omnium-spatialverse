# OMNIUM Spatialverse #



### What is OMNIUM Spatialverse? ###

**OMNIUM** (**O**verall **M**etrics & **N**etworking of **I**deas **U**nified for the **M**asses) **Spatialverse** 
is a 3D collaborative virtual space that integrates with communication platforms and other virtual environments.  